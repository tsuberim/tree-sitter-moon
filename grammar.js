const identifier = /[a-z_][a-z_0-9]*/

function sep1(expr, delimiter) {
  return seq(expr, repeat(seq(delimiter, expr)), optional(delimiter))
}

function sep(expr, delimiter) {
  return optional(sep1(expr, delimiter))
}

module.exports = grammar({
  name: 'moon',

  rules: {
    block: $ => seq('<', repeat1($._expr), '>'),
    var: $ => identifier, 
    num: $ => /\d+/,
    bool: $ => choice('false', 'true'),
    str: $ => seq('`', sep1(/[a-zA-Z0-9]*/, seq('{', $._expr, '}')),  '`'),
    def: $ => seq($.var, '=', $._expr),
    map: $ => seq('{', sep(seq('[', $._expr, ']', ':', $._expr), ','), '}'),
    app: $ => prec.left(1, seq($._expr, '(', sep($._expr, ','), ')')),
    fun: $ => seq(optional(seq('\\', sep($.var, ','))), '->', $._expr),
    _expr: $ => choice($.var, $.num, $.bool, $.str, $.def, $.map, $.app, $.fun, $.block, seq('(', $._expr, ')')),
  }
});